/* eslint-disable import/order */
import ReactDOM from 'react-dom/client';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import App from '@pages/App';
import Language from '@containers/Language';
import { store, persistor } from './redux/store';
import '@styles/core.scss';
import { ChakraProvider } from '@chakra-ui/react';

ReactDOM.createRoot(document.getElementById('root')).render(
  <Provider store={store}>
    <PersistGate persistor={persistor}>
      <ChakraProvider>
        <Language>
          <App />
        </Language>
      </ChakraProvider>
    </PersistGate>
  </Provider>
);
