import Advanced from '@components/advanced';
import Boost from '@components/boost';
import Footer from '@components/footer';
import Home from '@components/home';
import Navbar from '@components/navbar';
import '../../components/locales/i18n';

const App = () => (
  <div>
    <Navbar />
    <Home />
    <Advanced />
    <Boost />
    <Footer />
  </div>
);

export default App;
