import { Box, Heading, Stack, Text } from '@chakra-ui/react';
import { useTranslation } from 'react-i18next';
import FeatureCard from './card';
import Short from './short';
import IconBrandRecognition from '../assets/icon/IconBrandRecognition';
import IconDetailedRecords from '../assets/icon/IconDetailedRecords';
import IconFullyCustomizable from '../assets/icon/IconFullyCustomizable';

const Advanced = () => {
  const { t } = useTranslation();
  return (
    <Box bg="hsl(230, 25%, 95%)" pb={{ base: 20, lg: 32 }} px="6" role="region" aria-label="Services section">
      <Short />
      <Box textAlign="center">
        <Box maxW="540px" mx="auto" mb={{ base: 20, lg: 28, xl: 32 }}>
          <Heading as="h3" variant="h2" mb="4">
            {t('ad')}
          </Heading>
          <Text textStyle="medium">{t('textad')}</Text>
        </Box>
        <Box pos="relative" maxW="1110px" mx="auto">
          <Stack
            spacing={{ base: 24, lg: 8 }}
            zIndex="overlay"
            pos="relative"
            align="center"
            justify="center"
            direction={{ base: 'column', lg: 'row' }}
          >
            <FeatureCard
              index={1}
              icon={<IconBrandRecognition color="hsl(180, 66%, 49%)" boxSize="35px" />}
              title={t('title1')}
              text={t('text1')}
            />
            <FeatureCard
              index={2}
              icon={<IconDetailedRecords color="hsl(180, 66%, 49%)" boxSize="35px" />}
              title={t('title2')}
              text={t('text2')}
            />
            <FeatureCard
              index={3}
              icon={<IconFullyCustomizable color="hsl(180, 66%, 49%)" boxSize="35px" />}
              title={t('title3')}
              text={t('text3')}
            />
          </Stack>
          <Box
            w={{ base: '8px', lg: 'full' }}
            h={{ base: 'full', lg: '8px' }}
            pos="absolute"
            top={{ base: 0, lg: '50%' }}
            bg="hsl(180, 66%, 49%)"
            left={{ base: '50%' }}
            transform="translate(-50%,0)"
            zIndex="base"
          />
        </Box>
      </Box>
    </Box>
  );
};

export default Advanced;
