/* eslint-disable no-shadow */
import { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import axios from 'axios';
import { v4 as uuid } from 'uuid';
import {
  VStack,
  Box,
  Text,
  FormControl,
  Input,
  Button,
  GridItem,
  Grid,
  Flex,
  FormErrorMessage,
} from '@chakra-ui/react';
import { useTranslation } from 'react-i18next';
import Swal from 'sweetalert2';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { addShortenedLink, deleteShortenedLink } from '../redux/short';
import foto from '../assets/bg-shorten-desktop.svg';

const Short = () => {
  const { t } = useTranslation();
  const [inputUrl, setInputUrl] = useState('');
  const [selectedLink, setSelectedLink] = useState(null);
  const dispatch = useDispatch();
  const shortenedLinks = useSelector((state) => state.shortenedLinksSlice) || [];
  const [error, setError] = useState('');
  const [loading, setLoading] = useState(false);

  const getShortenedLink = async (url) => {
    try {
      const response = await axios.post(`https://api.shrtco.de/v2/shorten?url=${url}`);
      setError('');
      return response.data.result.full_short_link;
    } catch (error) {
      if (error.response?.data?.error_code) {
        const errorCode = error.response.data.error_code;
        if (errorCode === 1) {
          setError(t('ec1'));
        } else if (errorCode === 2) {
          setError(t('ec2'));
        } else if (errorCode === 3) {
          setError(t('ec3'));
        } else if (errorCode === 4) {
          setError(t('ec4'));
        } else if (errorCode === 5) {
          setError(t('ec5'));
        } else if (errorCode === 6) {
          setError(t('ec6'));
        } else if (errorCode === 7) {
          setError(t('ec7'));
        } else if (errorCode === 8) {
          setError(t('ec8'));
        } else if (errorCode === 9) {
          setError(t('ec9'));
        } else if (errorCode === 10) {
          setError(t('ec10'));
        } else {
          setError(`API Error: ${errorCode}`);
        }
      } else {
        setError('An error occurred. Please try again later.');
      }
    }
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);

    const isDuplicate = shortenedLinks.some((link) => link.originalLink === inputUrl);
    if (isDuplicate) {
      setError(t('ec11'));
      setLoading(false);
      return;
    }

    if (shortenedLinks.length >= 5) {
      // Show an alert here
      Swal.fire({
        title: 'Maximum Limit Reached',
        text: 'You can only add up to 5 shortened links.',
        icon: 'warning',
        confirmButtonColor: '#d33',
        confirmButtonText: 'OK',
      });
      setLoading(false);
      return;
    }

    const shortLink = await getShortenedLink(inputUrl);
    if (!shortLink) {
      setLoading(false);
      return;
    }

    const id = uuid();
    const newShortenedLink = {
      originalLink: inputUrl,
      shortenedLink: shortLink,
      id,
    };
    dispatch(addShortenedLink(newShortenedLink));
    setInputUrl('');
    setLoading(false);
  };

  const handleInputChange = (e) => {
    setInputUrl(e.target.value);
    setError('');
    setLoading(false);
  };

  const handleDeleteLink = (id) => {
    Swal.fire({
      title: 'Are you sure?',
      text: 'This action will permanently remove the todo item.',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d33',
      cancelButtonColor: '#3085d6',
      confirmButtonText: 'Yes, remove it!',
    }).then((result) => {
      if (result.isConfirmed) {
        dispatch(deleteShortenedLink(id));
      }
    });
  };

  return (
    <Box px="6" bg="#EEF1F7">
      <Box
        maxW="1200px"
        w="full"
        position="relative"
        top={{ base: '-86px', md: '-70px', xl: '-94px' }}
        margin="auto"
        marginTop={{ base: '40%', md: '10%' }}
      >
        <Box bg="hsl(257, 27%, 26%)" bgImage={foto} bgSize="cover" p={{ base: 6, md: 10, xl: 16 }} rounded="lg">
          <form onSubmit={onSubmit}>
            <FormControl id="link" isInvalid={error}>
              <Flex direction={{ base: 'column', md: 'row' }} align="center">
                <Input
                  placeholder={t('placeholder')}
                  fontSize={{ md: '18px', xl: '20px' }}
                  color="#34313E"
                  py={{ base: '22px', md: '28px', xl: '28px' }}
                  px={{ base: '14px', md: '16px', xl: '28px' }}
                  id="link"
                  bg="white"
                  value={inputUrl}
                  onChange={handleInputChange}
                />
                {error && (
                  <FormErrorMessage
                    fontSize="xs"
                    color="red"
                    mt="0"
                    display={{ base: 'block', md: 'none' }}
                    fontStyle="italic"
                  >
                    {error.response?.data?.error_code || error}
                  </FormErrorMessage>
                )}
                <Button
                  type="submit"
                  ml={{ base: 0, md: 4, xl: 6 }}
                  mt={{ base: 4, md: 0 }}
                  bg="hsl(180, 66%, 49%)"
                  w={{ base: 'full', md: 'auto' }}
                  color="white"
                  size={{ base: 'md', md: 'lg' }}
                  isLoading={loading}
                  loadingText="Loading..."
                >
                  {loading ? null : t('shorten')}
                </Button>
              </Flex>
              {error && (
                <FormErrorMessage
                  pos="absolute"
                  fontSize={{ md: 'sm', xl: 'md' }}
                  fontStyle="italic"
                  mt={{ md: 0, xl: 1 }}
                  display={{ base: 'none', md: 'block' }}
                  color="red"
                >
                  {error.response?.data?.error_code || error}
                </FormErrorMessage>
              )}
            </FormControl>
          </form>
        </Box>
        {shortenedLinks.length > 0 && (
          <VStack spacing="4" mt="6" w="full">
            {shortenedLinks.slice(0, 5).map((linkObj, index) => (
              <Grid
                templateRows={{ base: 'repeat(2,1fr)', lg: 'repeat(1,1fr)' }}
                templateColumns="repeat(12,1fr)"
                key={index}
                bg="white"
                w="full"
                rounded="md"
                pb={{ base: 4, md: 0, xl: 4 }}
                pt={{ xl: 4 }}
                alignItems="center"
              >
                <Box position="absolute" right="4" top="4" display="none">
                  <FontAwesomeIcon icon="fa-solid fa-scissors" />
                </Box>
                <GridItem
                  colSpan={{ base: 12, md: 9, xl: 6 }}
                  order="1"
                  py="4"
                  px={{ base: 4, md: 4, xl: 6 }}
                  borderBottom={{ base: '1px', xl: 0 }}
                  borderBottomColor="#E7E6E9"
                >
                  <Text textStyle="medium" color="black" fontWeight="bold" wordBreak="break-all" isTruncated>
                    {linkObj.originalLink}
                  </Text>
                </GridItem>
                <GridItem
                  rowSpan={2}
                  colSpan={{ base: 12, md: 3, xl: 1 }}
                  order={{ base: '3', md: '2', xl: '3' }}
                  px={{ base: 4, md: 6, xl: 6 }}
                >
                  <Button
                    rounded="lg"
                    w={{ base: 'full' }}
                    bgColor="red"
                    color="white"
                    onClick={() => handleDeleteLink(linkObj.id)}
                  >
                    Delete
                  </Button>
                </GridItem>
                <GridItem
                  rowSpan={2}
                  colSpan={{ base: 12, md: 3, xl: 1 }}
                  order={{ base: '3', md: '2', xl: '3' }}
                  px={{ base: 4, md: 6, xl: 6 }}
                >
                  <Button
                    rounded="lg"
                    w={{ base: 'full' }}
                    bgColor={selectedLink && linkObj.id === selectedLink.id ? 'black' : 'hsl(180, 66%, 49%)'}
                    color="white"
                    onClick={() => {
                      navigator.clipboard.writeText(linkObj.shortenedLink);
                      setSelectedLink(linkObj);
                    }}
                  >
                    {selectedLink && linkObj.id === selectedLink.id ? t('copy') : t('copied')}
                  </Button>
                </GridItem>
                <GridItem colSpan={{ base: 12, md: 9, xl: 4 }} order="2" py="2" px={{ base: 4, md: 4 }}>
                  <Text textStyle="medium" color="hsl(180, 66%, 49%)" wordBreak="break-all" textAlign={{ xl: 'right' }}>
                    {linkObj.shortenedLink}
                  </Text>
                </GridItem>
              </Grid>
            ))}
          </VStack>
        )}
      </Box>
    </Box>
  );
};

export default Short;
