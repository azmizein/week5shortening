import { Box, Button, Flex, Image, Stack, Text } from '@chakra-ui/react';
import { useTranslation } from 'react-i18next';
import foto from '../assets/illustration-working.svg';

const Home = () => {
  const { t } = useTranslation();
  return (
    <Flex
      align="center"
      justify="space-between"
      wrap="wrap"
      padding="1.5rem"
      bg="white"
      marginLeft={{ base: '0%', md: '10%' }}
      height={{ base: 'auto', md: 'auto' }}
      flexDir={{ base: 'column', md: 'row' }}
    >
      <Box
        display="flex"
        flexDirection="column"
        order={{ base: 2, md: 1 }}
        padding={{ base: '0 1rem', md: '0' }}
        width={{ base: '100%', md: 'auto' }}
        textAlign={{ base: 'center', md: 'left' }}
      >
        <Stack width={{ base: '100%', md: '500px' }}>
          <Text color="hsl(255, 11%, 22%)" fontSize={{ base: '40px', md: '70px' }} fontWeight="bold">
            {t('h2')}
          </Text>
          <Text color="hsl(0, 0%, 75%)">{t('text')}</Text>
          <Button
            marginTop="5%"
            bg="hsl(180, 66%, 49%)"
            borderRadius="50px"
            padding="8px 25px"
            width="150px"
            color="white"
            mx="auto"
          >
            {t('get')}
          </Button>
        </Stack>
      </Box>
      <Image src={foto} alt="foto" order={{ base: 1, md: 2 }} width={{ base: '100%', md: 'auto' }} />
    </Flex>
  );
};

export default Home;
