/* eslint-disable react/prop-types */
import { Box, Heading, Text } from '@chakra-ui/layout';

const FeatureCard = ({ title, text, icon, index }) => (
  <Box
    bg="white"
    rounded="md"
    pb="10"
    px="8"
    maxW="350px"
    textAlign={{ base: 'center', lg: 'left' }}
    pos="relative"
    top={{ base: 'unset', lg: index === 1 ? '-42px' : 'unset' }}
    bottom={{ base: 'unset', lg: index === 3 ? '-42px' : 0 }}
    border="2px solid black"
  >
    <Box
      p="6"
      bg="primary.darkpurple"
      rounded="full"
      display="inline-block"
      position="relative"
      top="-42px"
      border="2px solid black"
      backgroundColor="hsl(257, 27%, 26%)"
    >
      {icon}
    </Box>
    <Heading as="h4" variant="h4" fontSize="20px" mb="4">
      {title}
    </Heading>
    <Text fontSize="14px" color="hsl(0, 0%, 75%)">
      {text}
    </Text>
  </Box>
);

export default FeatureCard;
