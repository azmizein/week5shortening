import { Button, Flex, Text } from '@chakra-ui/react';
import { useTranslation } from 'react-i18next';
import foto from '../assets/bg-boost-desktop.svg';

const Boost = () => {
  const { t } = useTranslation();
  return (
    <Flex
      align="center"
      flexDirection="column"
      backgroundImage={foto}
      backgroundPosition="center"
      backgroundSize="cover"
      backgroundColor="hsl(257, 27%, 26%)"
      minH="150px"
      py={{ base: 24, lg: 16 }}
      px="6"
    >
      <Text fontSize="25px" color="white" fontWeight="bold">
        {t('boost')}
      </Text>
      <Button mt="10px" bg="hsl(180, 66%, 49%)" borderRadius="50px" padding="8px 25px" width="150px" color="white">
        {t('get')}
      </Button>
    </Flex>
  );
};

export default Boost;
