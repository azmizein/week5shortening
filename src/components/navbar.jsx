import { useState } from 'react';
import {
  Flex,
  Box,
  IconButton,
  Stack,
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  Text,
  Button,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalCloseButton,
  FormControl,
  FormLabel,
  Input,
  Image,
  MenuDivider,
} from '@chakra-ui/react';
import { ChevronDownIcon, HamburgerIcon } from '@chakra-ui/icons';
import { useTranslation } from 'react-i18next';
import logo from '../assets/logo.svg';
import users from '../user.json';

const Navbar = () => {
  const { t, i18n } = useTranslation();
  const [selectedLanguage, setSelectedLanguage] = useState('en');
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [isLoginModalOpen, setIsLoginModalOpen] = useState(false);

  const handleLogin = () => {
    const matchedUser = users.users.find((user) => user.username === username && user.password === password);
    if (matchedUser) {
      setIsLoggedIn(true);
      setIsLoginModalOpen(false);
    } else {
      alert('Invalid username or password. Please try again.');
    }
  };

  const handleLogout = () => {
    setIsLoggedIn(false);
    setUsername('');
    setPassword('');
  };

  const handleLanguageChange = (language) => {
    setSelectedLanguage(language);
    i18n.changeLanguage(language);
  };

  return (
    <Flex
      as="nav"
      align="center"
      justify="space-between"
      wrap="wrap"
      padding="1.5rem"
      bg="white"
      marginRight="10%"
      marginLeft="10%"
    >
      <Flex align="center" mr={4}>
        <Image src={logo} alt="logo" />
        <Box display={{ base: 'none', md: 'block' }} marginLeft="30px">
          <Stack direction="row" spacing={4} color="hsl(0, 0%, 75%)" fontWeight="bold">
            <Text>{t('features')}</Text>
            <Text>{t('pricing')}</Text>
            <Text>{t('resources')}</Text>
          </Stack>
        </Box>
      </Flex>

      {isLoggedIn ? (
        <Box display={{ base: 'none', md: 'block' }}>
          <Stack direction="row" spacing={4}>
            <Button bg="white" color="hsl(0, 0%, 75%)" onClick={handleLogout}>
              {t('logout')}
            </Button>
          </Stack>
        </Box>
      ) : (
        <Box display={{ base: 'none', md: 'block' }}>
          <Stack direction="row">
            <Menu>
              <MenuButton
                as={Button}
                rightIcon={<ChevronDownIcon />}
                width="80px"
                backgroundColor="white"
                color="hsl(0, 0%, 75%)"
              >
                {selectedLanguage === 'en' ? 'En' : 'Id'}
              </MenuButton>
              <MenuList>
                <MenuItem onClick={() => handleLanguageChange('en')}>En</MenuItem>
                <MenuItem onClick={() => handleLanguageChange('id')}>Id</MenuItem>
              </MenuList>
            </Menu>
            <Button bg="white" color="hsl(0, 0%, 75%)" onClick={() => setIsLoginModalOpen(true)}>
              {t('login')}
            </Button>
            <Button bg="hsl(180, 66%, 49%)" borderRadius="50px" padding="8px 25px" color="white">
              {t('logout')}
            </Button>
          </Stack>
        </Box>
      )}

      <Box display={{ base: 'block', md: 'none' }}>
        <Menu>
          <MenuButton as={IconButton} icon={<HamburgerIcon />} variant="outline" />
          <MenuList>
            <MenuItem>Features</MenuItem>
            <MenuItem>Pricing</MenuItem>
            <MenuItem>Resources</MenuItem>
            <MenuDivider />
            <Flex flexDirection="column">
              <Button bg="white" color="hsl(0, 0%, 75%)" onClick={() => setIsLoginModalOpen(true)}>
                {t('login')}
              </Button>
              <Button bg="hsl(180, 66%, 49%)" borderRadius="50px" padding="8px 25px" color="white">
                {t('logout')}
              </Button>
            </Flex>
          </MenuList>
        </Menu>
      </Box>

      <Modal isOpen={isLoginModalOpen} onClose={() => setIsLoginModalOpen(false)}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Login</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <FormControl>
              <FormLabel>Username</FormLabel>
              <Input type="text" value={username} onChange={(e) => setUsername(e.target.value)} />
            </FormControl>
            <FormControl mt={4}>
              <FormLabel>Password</FormLabel>
              <Input type="password" value={password} onChange={(e) => setPassword(e.target.value)} />
            </FormControl>
            <Button mt={4} colorScheme="teal" onClick={handleLogin}>
              Login
            </Button>
          </ModalBody>
        </ModalContent>
      </Modal>
    </Flex>
  );
};

export default Navbar;
