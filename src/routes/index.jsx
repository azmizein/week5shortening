import MainLayout from '@layouts/MainLayout';
import App from '@pages/App';
import NotFound from '@pages/NotFound';

const routes = [
  {
    path: '/',
    name: 'App',
    protected: false,
    component: App,
    // layout: MainLayout,
  },

  { path: '*', name: 'Not Found', component: NotFound, layout: MainLayout, protected: false },
];

export default routes;
