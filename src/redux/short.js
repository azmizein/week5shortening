import { createSlice } from '@reduxjs/toolkit';

const shortenedLinksSlice = createSlice({
  name: 'shortenedLinks',
  initialState: [],
  reducers: {
    addShortenedLink: (state, action) => {
      state.unshift(action.payload);
    },
    deleteShortenedLink: (state, action) => state.filter((link) => link.id !== action.payload),
  },
});

export const { addShortenedLink, deleteShortenedLink } = shortenedLinksSlice.actions;
export default shortenedLinksSlice.reducer;
